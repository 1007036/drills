# Drill

### Circuits

##### Right circuits in VH-AFR, runway 28R

* take-off
* **set full power**
* **55KIAS rotate**
  * apply right rudder to keep balance
* **300ft** after take-off check
  * **flaps up**
  * **power is full**
  * **> 2300rpm**
  * **landing light** [can leave on for circuits]
  * achieve **74KIAS** (Vy)
* at **500ft** turn onto crosswind
  * **lookout** [for right turn]: clear left, clear centre, clear above, clear right
  * roll out when heading bug at 90 degrees
  * maximum **AoB 15 degrees** [in climb]
  * adjust **attitude for 74KIAS** (Vy)
  * **level off at 1000ft**
* **at 1nm from runway turn onto downwind**
  * **lookout** [for right turn]: clear left, clear centre, clear above, clear right 
  * AoB 30 degrees
  * rollout to straight & level when heading bug at 180 degrees
  * radio `ALPHA FOXTROT ROMEO DOWNWIND FOR TOUCH AND GO`
  * **BUMFISH check**
    * **Brakes**
    * **Under-carriage**
    * **Mixture full rich**
    * **Fuel on both and sufficient to go-around**
    * **Instruments in green**
    * **Switches, landing light ON**
    * **Hatches and Harnesses secure**
  * hold the heading with visual reference point
  * workflow: Height, Speed, Heading, Spacing
  * **abeam runway threshold reduce power to 2000rpm**
  * maintain 1000ft
  * **at below 110KIAS take [first stage] 10 flap**
  * trim for flap
* **at 45 degrees from runway threshold turn onto base**
  * lookout [for right turn]: clear left, clear centre, clear above, clear right
  * roll out when heading bug at 270 degrees
  * **power back** to 1500rpm
  * maximum **AoB 30 degrees** [descent]
  * **at below 85KIAS take [second stage] 20 flap**
  * trim for flap
  * half-sky/half-ground attitude
  * aim for **75KIAS in approach descent**
* **turn onto final**
  * aim for **75KIAS in approach**
  * tower `ALPHA FOXTROT ROMEO RUNWAY TWO EIGHT RIGHT CLEAR TOUCH AND GO`
  * radio `TWO EIGHT RIGHT CLEAR TOUCH AND GO ALPHA FOXTROT ROMEO`
  * aimpoint (runway numbers), aspect, airspeed (65)
  * **take [full] 30 flap**
  * trim for flap
  * **aim for 65KIAS**
* land
  * **at just before threshold markers, power to idle**
  * **flare**: straight and level attitude, look to the distant runway
  * **hold off** with nose up, horizon at dash cowling
* after landing
  * **flaps up**
