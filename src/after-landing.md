# Drill

### After landing

* exit to taxiway
* flaps UP
* aircraft lights
  * beacon light ON
  * landing light OFF
  * taxi light ON
  * nav light ON
  * strobe light OFF
* transponder STANDBY
* trim SET to TAKE OFF
* radio `ARCHER GROUND ALPHA FOXTROT ROMEO REQUEST TAXI TO THE EASTERN APRON`
* tower `ALPHA FOXTROT ROMEO CLEARED FOR TAXI TO THE EASTERN APRON`

##### Engine shut off

* parking brake SET
* electrical equipment OFF
  * all lights except navigation beacon OFF
* avionics master OFF
* mixture IDLE CUT OFF
* master switch OFF
* magnetos switch OFF
* control lock INSTALL
* fuel selector valve LEFT or RIGHT to prevent cross feed
* pitot tube cover
