# Drill

### Pre-circuits runway 10L

##### Taxi to run-up bay

* receive ATIS on 120.9
* **set QNH on altimeter and CHECK ~60ft**
* set ground tower 119.9
* **aircraft lights**
  * beacon light ON
  * landing light OFF
  * taxi light ON
  * nav light ON
  * strobe light OFF
* check brakes
* radio `ARCHER GROUND ALPHA FOXTROT ROMEO CESSNA ONE SEVEN TWO EASTERN APRON
  DUAL RECEIVED ATIS FOR CIRCUITS REQUEST TAXI`
* tower `ALPHA FOXTROT ROMEO TAXIWAY BRAVO CROSS ZERO FOUR RIGHT AND ZERO FOUR
  LEFT HOLDING POINT BRAVO FIVE RUNWAY ONE ZERO LEFT`
* radio `TAXIWAY BRAVO CROSS ZERO FOUR RIGHT AND ZERO FOUR LEFT HOLDING POINT
  BRAVO FIVE RUNWAY ONE ZERO LEFT ALPHA FOXTROT ROMEO`
* taxi to run-up bay

##### Pre take-off checks

* parking brake SET
* seat belts SECURE
* cabin doors CLOSED and LOCKED
* flight controls FREE and CORRECT
* flight instruments CHECK and SET
  * **heading indicator to magnetic compass**
  * **altimeter QNH**
* fuel quantity CHECK
* mixture RICH
* fuel selector BOTH
* magnetos CHECK
  * throttle 1800rpm
  * should not exceed 150rpm drop on each mag
  * should not exceed 50rpm difference between mags
  * vacuum gauge CHECK
  * engine pressure CHECK
  * engine temperature CHECK
* annunciator panel NONE
* throttle CHECK IDLE
* throttle 1000rpm OR LESS
* throttle friction lock ADJUST
* ~~strobe light ON~~
* radios and avionics SET
* elevator trim SET FOR TAKE OFF
* wing flaps ZERO
* brakes RELEASE

##### Departure briefing

> Today we will be using runway one zero left which will require a left hand
  circuit direction.

##### Safety briefing

> In the event of FIRE, FAILURE or ABNORMALITY prior to rotate I WILL close the
  throttle, apply maximum braking, exit the runway at the closest taxiway.
> In the event of FIRE, FAILURE or ABNORMALITY after rotate with remaining
  runway I WILL lower the nose, CLOSE the throttle, land the aircraft on the
  remaining runway, apply maximum braking, exit the runway at the closest
  taxiway.
> In the event of FIRE, FAILURE or ABNORMALITY after rotate WITHOUT remaining
  runway I WILL lower the nose to adopt the glide attitude, select a landing
  area within 30 degrees either side of the nose the first time I will attempt a
  turn back to the runway will be only if I have commenced a turn OR have
  sufficient height to glide back to the runway.  

##### Taxi to holding point

* strobe light ON
* landing light ON
* transponder ON 3000
* radio `ARCHER TOWER ALPHA FOXTROT ROMEO HOLDING POINT BRAVO FIVE RUNWAY ONE
  ZERO LEFT CIRCUITS READY`
* tower `ALPHA FOXTROT ROMEO CLEARED FOR TAKE OFF ON RUNWAY ONE ZERO LEFT`
* radio `CLEARED FOR TAKE OFF ON RUNWAY ONE ZERO LEFT ALPHA FOXTROT ROMEO`
