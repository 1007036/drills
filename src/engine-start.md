# Drill

### Engine Start

##### Cessna 172S

* checks
  * pre-flight inspection COMPLETE
  * passenger briefing COMPLETE
  * seats and seat belts ADJUST and LOCK. Ensure inertia reel locking.
  * brakes TEST and SET
  * circuit breakers CHECK IN
  * electrical equipment OFF
  * avionics master switch OFF
  * fuel selector valve BOTH
  * fuel shutoff valve ON (push full in)
  * avionics circuit breakers CHECK IN

* prime engine with fuel
  * throttle to 1/2
  * master switch ON
  * beacon light ON
  * fuel pump ON
  * mixture full-rich until stable fuel flow indicated (3 to 5 seconds)
  * mixture full-lean
  * fuel pump OFF

* engine start
  * mixture full-lean
  * throttle to 1/4
  * hand on mixture
  * "CLEAR PROP"
  * crank engine to start
  * mixture to full-rich

* check
  * oil pressure
  * navigation beacon ON
  * avionics master switch ON
  * radios ON
  * flaps UP
  