#!/usr/bin/env runhaskell

{-# OPTIONS_GHC -Wall #-}

import System.Environment
import Text.Printf

isatemp ::
  Double
isatemp =
  15.0

isapressure ::
  Double
isapressure =
  1013.2

pa ::
  Double
  -> Double
  -> Double
pa qnh ele =
  ele + ((isapressure - qnh) * 30)

pa_da ::
  Double
  -> Double
  -> Double
pa_da pa' temp =
  (temp - isatemp + (pa' / 500)) * 120 + pa'

da ::
  Double
  -> Double
  -> Double
  -> Double
da qnh ele temp =
  pa_da (pa qnh ele) temp

main ::
  IO ()
main =
  do  a <- getArgs
      case a of
        (q:e:t:_) ->
          let q' = read q
              e' = read e
              t' = read t
              pa' = pa q' e'
              da' = da q' e' t'
              pa'' = printf "%0.1f" pa'
              da'' = printf "%0.1f" da'
          in putStrLn ("Pressure Altitude=" ++ pa'' ++ " Density Altitude=" ++ da'')
        _ ->
          putStrLn "<qnh> <ele> <temp>"
